#!/bin/bash
heading() {

        printf "\n===================================\n"
        printf "$1 \n"
        printf "===================================\n\n"
}

set_term_default(){


    cp "$settings_path/settings.json" "$settings_path/settings.json.bak"

    while read line; do
        if [[ "$line" == *"guid"* ]]; then
            guid=$line
        fi

        if [[ "$line" == *"Ubuntu 22.04.1"* ]]; then
            break
        fi
    done < "$settings_path/settings.json.bak"

    font_string="\"font\": \n {\n \"face\": \"CaskaydiaCove Nerd Font Mono\"\n },"

    my_number=`echo "$guid" | sed -E "s/.*\"\{(.*)\}\".*/\1/g"`

    sed -E "/.*$my_number.*/i $font_string" "$settings_path/settings.json" > "$settings_path/font-settings.json"

    sed -E "s/.*defaultProfile.*/     \"defaultProfile\": \"\{$my_number}\",/" "$settings_path/settings.json.bak" > $settings_path/"settings.json"
}

win_username="$(powershell.exe '$env:UserName')";
term_path="/mnt/c/Users/${win_username%?}/AppData/Local/Packages"
term_name=`/bin/ls "$term_path" | grep Terminal`
settings_path="$term_path/$term_name/LocalState"


 git clone "https://gitlab.com/wuphysics87/hello-world"

 heading "Updating Cache and Packages"
 sudo apt update -y && sudo apt upgrade -y

 sudo apt install -y python3-pip python3 npm unzip zsh fontconfig \
     shellcheck tldr man-db entr cmake fontconfig \
     imagemagick python3-pip bat exa ripgrep fd-find

 curl -sS https://starship.rs/install.sh | sh


 set_term_default

 path="/mnt/c/Users/${win_username%?}"

 mkdir -p "$path/Documents/repos"
 sudo chmod 777 "$path/Documents/repos"
 rm -rf "$path/Documents/repos/hello-wor*"

 repo_dir="$HOME/hello-world"

 #mv "$HOME/hello-world" "$path/Documents/repos" > /dev/null 2>&1

 # Copy Shell Config
 cp "$repo_dir/shell/zshrc" "$HOME/.zshrc"
 cp "$repo_dir/shell/profile" "$HOME/.profile"
 cp "$repo_dir/shell/settings.json" "/mnt/c/Users/${win_username%?}/AppData/Roaming/VSCodium/User/settings.json"


 #cat "source $HOME/.hello-zshrc" >> "$HOME/.zshrc"

 mkdir -p "$HOME/.local/bin"
 cp "$repo_dir/shell/rerun" "$HOME/.local/bin"

 mkdir -p "$HOME/.config"
 cp "$repo_dir/shell/starship.toml" "$HOME/.config"

 rm -rf "$path/Documents/repos/hello-wo*"
 sudo rm -rf "$path/Documents/repos/hello-world"
 cp -r "$HOME/hello-world" "$path/Documents/repos" > /dev/null 2>&1

 python3 -m pip install --upgrade setuptools
 python3 -m pip install --upgrade pip

 # Install Linters
 npm install eslint css-lint remark-lint
 pip install pycodestyle jedi

if [ ! "$(grep 'CaskaydiaCove' settings.json)" ];then
   cp "$settings_path/settings.json" $settings_path/"settings.json.bak"
fi

 set_term_default

 chsh -s /bin/zsh

 rm ~/.zprofile

 codium "C:\\Users\\${win_username%?}\\Documents\\repos\\hello-world" &
 zsh
