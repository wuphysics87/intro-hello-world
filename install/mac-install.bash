#!/bin/bash

heading() {

        printf "\n===================================\n"
        printf "$1 \n"
        printf "===================================\n\n"
}

choice_prompt(){
    prompt="$1"
    runner="$2"

    heading "$prompt"

    printf "    (y)es\n    (n)o\n\n"
    read -r choice

    if [[ "$choice" == "y" ]]; then

        $runner

    elif [[ "$choice" == "n" ]]; then

        return

    else

        choice_prompt "$prompt" "$runner"

    fi
}

brew_uninstall(){
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/uninstall.sh)"
}

uninstall(){

    npm uninstall eslint css-lint remark-lint
    pip3 uninstall -y pycodestyle jedi
    rm -rf ~/.zinit ~/.hello-profile ~/.hello-zshrc ~/.pulsar \
        ~/.cargo ~/github/hello-world ~/.zprofile ~/.zshenv ~/.config/starship.toml

    choice_prompt "Would you like to uninstall your brew packages?" \
        "brew uninstall git node python@3.8 unzip shellcheck tldr man-db entr zsh cmake python starship bat exa ripgrep fd imagemagick vscodium"

    choice_prompt "Would you like to uninstall the brew package manager?" "brew_uninstall"
    choice_prompt "Would you like to uninstall codium packages?" "codium --list-extensions | xargs -I % codium --uninstall-extension %"

    if [ "$(command -v codium)" ]; then
        sudo rm -rf "/Applications/VSCodium"
    fi

    if [ -d "/Applications/Pulsar*" ]; then
        rm -rf "$HOME/.pulsar"
        sudo rm -rf "/Applications/Pulsar*"
    fi

    rm  "$HOME/github/hello-world" "$HOME/Documents/repos/hello*" "$HOME/Downloads/hello*" "$HOME/Downloads/hello*"  "$HOME/.pulsar" > /dev/null 2>&1

}

install(){

    heading "Begin Mac Install"
    echo 'PATH="$PATH:$HOME/bin:$HOME/.cargo/bin:$HOME/.local/bin:/opt/homebrew/bin/"' >> "$HOME/.zprofile"
    source "$HOME/.zprofile"

        echo 'eval "$(/opt/homebrew/bin/brew shellenv)"' >> ~/.zprofile
    ### BREW INSTALL ###
    if [ ! "$(command -v brew)" ]; then
        heading "Installing Brew"

        /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
        echo 'eval "$(/opt/homebrew/bin/brew shellenv)"' >> ~/.zprofile
        eval "$(/opt/homebrew/bin/brew shellenv)"
    fi

    ### BREW PACKAGES INSTALL ###
    brew install --cask vscodium
    codesign --sign - --force --deep /Applications/VSCodium.app
    xattr -d com.apple.quarantine /Applications/VSCodium.app

    heading "Installing Brew Packages"
    brew install git node unzip shellcheck tldr man-db \
    entr cmake starship bat exa ripgrep fd wget imagemagick
    brew tap homebrew/cask-fonts && brew install --cask font-hack-nerd-font
    brew tap homebrew/cask-fonts && brew install --cask font-fira-code
    brew tap homebrew/cask-fonts && brew install --cask font-Fira-Code-nerd-font

    ### CODIUM PACKAGE INSTALL ###

    c_packages=("dbaeumer.vscode-eslint" "mads-hartmann.bash-ide-vscode" "magicstack.MagicPython" "tht13.python" "timonwong.shellcheck")
    for package in "${c_packages[@]}"; do
        codium --install-extension "$package" > /dev/null 2>&1
    done

    ### UPGRADE PIP ###
    python3 -m pip install --upgrade pip
    python3 -m pip install --upgrade setuptools

    ### Install Linters ###
    npm install eslint css-lint remark-lint
    pip3 install pycodestyle jedi


    ### CLONE REPO ###
    repo_dir="$HOME/Documents/repos/hello-world"
    rm -rf "$repo_dir"
    mkdir -p "$HOME/Documents/repos"
    sudo chmod 777 "$HOME/Documents/repos"
    git clone "https://gitlab.com/wuphysics87/hello-world" "$repo_dir"

    # Copy Shell Config
    mkdir -p "$HOME/.vscode-oss/extensions/"
    cp "$repo_dir/shell/extensions.json" "$HOME/.vscode-oss/extensions/"
    cp "$repo_dir/shell/zshrc" "$HOME/.hello-zshrc"
    cp "$repo_dir/shell/profile" "$HOME/.hello-profile"

    if [ ! "$(grep ~/.hello-zshrc ~/.zshrc)" ]; then
        echo "source ~/.hello-zshrc" >> ~/.zshrc
    fi

    mkdir -p "$HOME/.local/bin"
    cp "$repo_dir/shell/rerun" "$HOME/.local/bin"

    mkdir -p "$HOME/.config"
    cp "$repo_dir/shell/starship.toml" "$HOME/.config"


    cp "$repo_dir/shell/mac-settings.json" "$HOME/Library/Application\ Support/VSCodium/User/settings.json"

    rm "$HOME/.zprofile"

    codesign --sign - --force --deep /Applications/VSCodium.app
    xattr -d com.apple.quarantine /Applications/VSCodium.app
    codium --install-extension dbaeumer.vscode-eslint mads-hartmann.bash-ide-vscode magicstack.MagicPython tht13.python timonwong.shellcheck
    codium "$HOME/Documents/repos/hello-world" &
    zsh
}

mac_cpu_type(){
    [ ! "$(command -v sysctl)" ] && echo "not mac" && exit
    if [ ! "$(sysctl -n machdep.cpu.brand_string | grep Intel)" ]; then
        echo "Apple Silicon"
    else
        echo "Intel"
    fi
}

cpu="$(mac_cpu_type)"
mac="\nYou have an $cpu processor."

clear
heading "Your operating system is MacOs.$mac\nWould you like to"
printf "Install    1\nUninstall  2\nReinstall  3\n: "
read -r choice

case "$choice" in
    1) install; heading "Install Complete!";;

    2) uninstall; heading "Uninstall Complete!" ;;

    3) uninstall; heading "Uninstall Complete!"
       printf "%s " "Press enter to continue"; read ans; install ;;

    *) printf "\nIllegal choice! Quitting\n\n" && exit

esac

