function WSL-InstallPrompt {
    Clear-Host
    Write-Output "-----------------------------------"
    Write-Output " Welcome to the Windows Installer! "
    Write-Output "       Choose Your Option          "
    Write-Output "-----------------------------------"
    Write-Output "Install    1"
    Write-Output "Uninstall  2"
    Write-Output "Reinstall  3"
    $Selection = Read-Host " "

    if ($Selection -eq "1") {
        WSL-Install
        Install-Complete
    }

    elseif ($Selection -eq "2"){
        WSL-Uninstall
    }

    elseif ($Selection -eq "3"){
        WSL-Uninstall
        WSL-Install
        Install-Complete
    }

    else {
        WSL-InstallPrompt

    }
}

function WSL-Install {

    if( -not ( Get-Command "choco" -ErrorAction SilentlyContinue )) {
        Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
    }

    choco feature enable -n=allowGlobalConfirmation

    # Install chocolatey packages
    choco install -y --nocolor -r python nodejs shellcheck microsoft-windows-terminal git vscodium cascadia-code-nerd-font nerd-fonts-3270 nerdfont-hack cmake make mingw

    python.exe -m pip install --upgrade pip

    # python jedi
    pip install  jedi pycodestyle
    npm install csslint eslint remark

    # Install WSL
    wsl --install Ubuntu-22.04 --no-launch
    wsl.exe --install Ubuntu-22.04 --no-launch
}

function Install-Complete {
        Clear-Host
        Write-Output ""
        Write-Output "-----------------------------------"
        Write-Output "   If you haven't already, open "
        Write-Output "   wluhello.world on your phone. "
        Write-Output " Continue from Step 7 After Reboot"
        Write-Output "-----------------------------------"
        Write-Host -NoNewLine 'Press any key to Reboot...';
        $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');
        Restart-Computer
}

function WSL-Uninstall {

    if(( Get-Command "pip" -ErrorAction 'silentlycontinue' )) {
        pip uninstall -y jedi pycodestyle
    }

    if(( Get-Command "npm" -ErrorAction 'silentlycontinue' )) {
        npm uninstall csslint eslint remark
    }

    # Uninstall Chocolatey Packages

    #if(( Get-Command "choco" -ErrorAction SilentlyContinue )) {
    #    Uninstall-Chocolatey
    #}
    choco feature enable -n=allowGlobalConfirmation
    if(( Get-Command "choco" -ErrorAction 'silentlycontinue' )) {
        Uninstall-ChocoPackages
    }

    # Remove WSL
    wsl --unregister Ubuntu-22.04
    wsl --unregister Ubuntu

    Clear-Host
    Write-Output "-----------------------------------"
    Write-Output "      Exiting Uninstaller"
    Write-Output "-----------------------------------"
    Write-Output " "
}

function Uninstall-Chocolatey {

    Write-Output ""
    Write-Output "-----------------------------------"
    Write-Output "    Uninstall Chocolatey? "
    Write-Output "-----------------------------------"
    Write-Output ""
    Write-Output "(y)es"
    Write-Output "(n)o"
    $Selection = Read-Host " "

    if ( $Selection -eq "yes"-or $Selection -eq "y" ) {

        rm \ProgramData\Chocolatey

    }

    elseif ( $Selection -eq "no" -or $Selection -eq "n" ) {
        return
    }

    else {

        Uninstall-Chocolatey

    }
}

function Uninstall-ChocoPackages {

    Write-Output ""
    Write-Output "-----------------------------------"
    Write-Output "    Uninstall Chocolatey Packages? "
    Write-Output "-----------------------------------"
    Write-Output ""
    Write-Output "(y)es"
    Write-Output "(n)o"
    $Selection = Read-Host " "

    if ( $Selection -eq "yes"-or $Selection -eq "y" ) {
        choco uninstall -y --nocolor -r python nodejs shellcheck microsoft-windows-terminal git vscodium cascadia-code-nerd-font wsl-ubuntu-2004 nodejs.install
    }

    elseif ( $Selection -eq "no" -or $Selection -eq "n" ) {
        return
    }

    else {

        Uninstall-ChocoPackages

    }
}

WSL-InstallPrompt
