#!/usr/bin/env python3
import matplotlib
import numpy
import plotly.graph_objects as go
import matplotlib.pyplot as plt
from pylab import *
from matplotlib import *
import sys

sys.path.insert(1, 'data')

def choose_again():

    print("\n============================")
    print("      Plot Again?")
    print("       Yes.....1 ")
    print("        No.....2 ")
    print("============================\n")
    choice= input(": ")
    if choice == '1':
        plot_prompt()

def data_choice():
    print("\n============================")
    print("Choose Your Dataset Number")
    print("        1, 2 or 3")
    print("============================\n")

def scatter_plot():
    data_choice()
    data_set = input(": ")
    if data_set in ['1', '2', '3']:
        scatter='scatter-' + data_set
        print("\n============================")
        print("Choose Polynomial Fit Order ")
        print("        1, 2 or 3")
        print("============================\n")
        poly_order= input(": ")

        if poly_order in ['1', '2', '3']:
            plot_data = __import__(scatter, fromlist=["main"])
            x = plot_data.x_axis
            y = plot_data.y_axis
            plt.scatter(x, y)
            plt.plot(np.unique(x), np.poly1d(np.polyfit(x, y, int(poly_order)))(np.unique(x)))
            plt.show()
            choose_again()
        else:
            print("\nIllegal choice! Pick again!\n")
            scatter_plot()
    else:
        print("\nIllegal choice! Pick again!\n")
        scatter_plot()

def contour_plot():
    data_choice()
    data_set = input(": ")

    if data_set in ['1', '2', '3']:
        sheet='sheet-' + data_set
        plot_data = __import__(sheet, fromlist=["main"])
        fig = go.Figure(data = go.Contour(z=plot_data.my_data))
        fig.show()
        choose_again()
    else:
        print("\nIllegal choice! Pick again!\n")
        contour_plot()

def plot_prompt():
    print("\n============================")
    print("  Choose Your Plot Type")
    print("     Scatter.....1 ")
    print("     Contour.....2 ")
    print("============================\n")
    plot_type = input(": ")

    if plot_type == '1':
        scatter_plot()
    elif plot_type == '2':
        contour_plot()
    else:
        print("\nIllegal choice! Pick again!\n")
        plot_prompt()

plot_prompt()
