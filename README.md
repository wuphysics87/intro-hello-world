<div align="center">

# Hello World 

</div>

This repository will automate the installation of a *[Professional Development Environment](https://www.ibm.com/topics/software-development)* on MacOS and Windows.

We'll get a [Background](#background) of our programming tools, [Automate Our Installation](#installation), and see [Hello World Programs](#hello-world-programs) in popular languages which can lead to several exciting career paths.

Let's get coding!

### Table of Contents

* [Background](#background) 
* [Installation](#installation)
* [Terminal Tools](#terminal-tools)
  * [The Command Line Interface](#the-command-line-interface)
  * [Git](#git)
* [Text Editor Tips](#text-editor-tips)
  * [Rendered Markdown](#rendered-markdown)
  * [Popup Terminal](#popup-terminal)
* [Hello World Programs](#hello-world-programs)
  * [HTML, CSS, And Javascript](#html-css-and-javascript)
  * [Bash](#bash)
  * [Python](#python)
  * [Language Comparison](#language-comparison)
  * [To Learn More](#to-learn-more)
* [Appendix](#appendix)
  * [Entering the BIOS](#entering-the-bios)
  * [What We Installed](#what-we-installed)
  * [Acknowledgements](#acknowledgements) 

## Background

Our programming environment will consist of several tools: a terminal, a shell, a text editor, git, and a few package managers. 

The terminal is where we type the commands to tell our computer what to do. It's more than a trope you'l see on [TV or in movies](https://www.youtube.com/watch?v=u8qgehH3kEQ); it's the most direct and extensible way to interface with our computer. 

<div align="center"><img src=./images/terminal.png width="500"/></div> 

&nbsp; &nbsp; &nbsp;

Shells run in terminals. When we are typing into a terminal, we are actually using a shell language. There are many types of shell, but perhaps the most powerful is the [UNIX shell](https://www.youtube.com/watch?v=dxIPcbmo1_U). MacOS and Linux come with UNIX shells out of the box. Windows users will need to install one. 

<div align="center"><img src=./images/unix-system.jpg width="500"/></div> 

&nbsp; &nbsp; &nbsp;

The text editor is where we write our programs. Historically, one's choice of text editor has been the source of [many debates](https://en.wikipedia.org/wiki/Editor_war). Outside of being fodder for flame wars, picking a text editor can be a daunting task as there are so many options. To simplify things, we've chosen [Codium](https://vscodium.com/), a Free and Open Source descendant of GitHub's famous [Atom](https://github.blog/2022-06-08-sunsetting-atom/), and a telemetry free fork Microsoft's VSCode. 

<div align="center"><img src=./images/text-editor.png width="500"/></div> 

&nbsp; &nbsp; &nbsp;

Git is an enormously powerful tool for version control as well as collaboration. It is the brain child of Linus Torvalds, the creator of Linux.  Linus hated working with other people because of how complex it could get. Think of all of the versions you have of an important paper.  And now consider working with a thousand people to create a single document. Git solves that problem. It also allows you to clone any one of millions of Git Repositories (or repos) from the internet, allowing you to reuse code someone has been gracious enough to make Open Source!

<div align="center"><img src=./images/git.jpeg width="500"/></div> 

&nbsp; &nbsp; &nbsp;

Package managers are the way the cool kids install new programs on their systems. Typically, installing new programs requires you to go to a website and download an executable or use a store like the Windows, Play, or App Store. With a package manager, you can install your programs by typing the name of the package manager, an install command, and the name of the package directly in your terminal. It is much faster than using a web browser, and it automates building complex code with external dependencies.

<div align="center"><img src=./images/package-manager-white.png width="500"/></div> 

&nbsp; &nbsp; &nbsp;

[Top](#hello-world)

## Installation

Be sure to read *ALL* the instructions for your operating system before beginning. Installation may take a while. Be patient and you will be rewarded!

Navigate to **`wluhello.world`** on your phone, as well as your computer, to have the instructions available on two screens. 

*This is especially important for Windows users.* 

# MacOS 

*NOTE*: Installation will pause if your computer goes to sleep. Be sure to move the mouse periodically to keep things moving.

1. Copy following command by hovering over the right side of the box and clicking the copy icon: 

``` sh
cd "$HOME"; rm -rf "Downloads/mac-install.bash"; curl -o Downloads/mac-install.bash "https://gitlab.com/wuphysics87/hello-world/-/raw/main/install/mac-install.bash?inline=false"; bash Downloads/mac-install.bash
```

&nbsp; &nbsp; &nbsp;

2. Press **`cmd+space`** to open Spotlight, type **`term`**, and press **`return`** to open a terminal.

 <img src=./images/mac-open-term.png width="500"/>

&nbsp; &nbsp; &nbsp;

3. Select the terminal with your mouse. Paste the command into by pressing **`cmd+v`**. Press **`return`** to run. 

 <div align="center"><img src=./images/mac-paste-command-crop.png width="500"/></div> 

&nbsp; &nbsp; &nbsp;

4. Once you run the command, you will be prompted to choose an installation option. Press **`1`** to install.

<div align="center"> <img src=./images/mac-install-prompt.png width="450"/></div>

&nbsp; &nbsp; &nbsp;

5. Next, `brew` will begin installing. Press **`return`** to continue.

<img src=./images/mac-brew-continue.png width="500"/>

&nbsp; &nbsp; &nbsp;



6. Enter your login password when prompted. *NOTE:* You won't see any characters as you enter your password. This is a security feature. 

<div align="center"><img src=./images/mac-enter-pw.png width="500"/></div>

**TROUBLESHOOTING:** If you are on a university computer, you may not be able to run the **`sudo`** command. You will have to visit the Help Desk in order to gain these permissions.

&nbsp; 

7. When installation has completed, this repository will open itself in **`Codium`**. Click the appropriate button to trust our code. 

8. Jump to [Finishing Up](#finishing-up) to ensure your installation was successful. 

Then hop on down to [Text Editor Tips](#text-editor-tips) to learn more about our tools before we start programming.


[Top](#hello-world)

# Windows 

**IMPORTANT:** Before beginning, navigate to **`wluhello.world`** on your phone's web browser. 

*You will be required to reboot, so it is important to have the instructions available at all times.*

&nbsp; &nbsp; &nbsp; 

## Windows Part 1

TODO: Rename VSCodium to Codium Local-disk/ProgramData/Microsoft/Windows/Start menu/Programs/VsCodium
1. Copy following command by hovering over the right side of the box and clicking the copy icon: 

 ``` powershell
 Set-ExecutionPolicy Bypass -Scope Process -Force; cd $home; Remove-Item Downloads\win-install*; Invoke-WebRequest -Uri https://gitlab.com/wuphysics87/hello-world/-/raw/main/install/win-install.ps1?inline=false -OutFile Downloads\win-install.ps1; Invoke-WebRequest -Uri https://gitlab.com/wuphysics87/hello-world/-/raw/main/install/win-install.bash?inline=false -OutFile Downloads\win-install.bash; .\Downloads\win-install.ps1
 ```

&nbsp; &nbsp; &nbsp; 

2. Press the **`win`** key or click on the start menu icon, type **`powershell`**, and then click **`Run as Administrator`**.

<img src=./images/powershell-admin.png width="500"/>

&nbsp; &nbsp; &nbsp;

3. Your screen will show the following warning window. Click **`Yes`** to continue.

<img src=./images/win-powershell-warning.png width="500"/>

&nbsp; &nbsp; &nbsp;

4. Right click in the Powershell window to paste the command from **`Step 1`**. Press **`enter`** to run.  Press **`A`** when prompted to allow the script to run.  


<div align="center"> <img src=./images/win-command-paste.png width="750"/></div>

&nbsp; &nbsp; &nbsp;

5. Choose install by pressing **`1`**. Press **`y`** followed by `enter` to accept any remaining prompts. 

<img src=./images/win-install-prompt.png width="400"/>

&nbsp; &nbsp; &nbsp;

6. Next, you will be promped to reboot with a final reminder to open these instructions on your phone. After reboot, open your computer's web browser, return to **`wluhello.world`**. Press `enter` when you ready to reboot.

<img src=./images/win-install-reboot.png width="400"/>

&nbsp; &nbsp; &nbsp;

## Windows Part 2

7. Copy the following by clicking on the copy icon on the right side of the box: 

``` sh
win_username="$(powershell.exe '$env:UserName')"; bash "/mnt/c/Users/${win_username%?}/Downloads/win-install.bash"
```

&nbsp; &nbsp; &nbsp;

8. Press the **`win`** key or click to open your start menu, type **`Ubuntu 22`**, and click **`Ubuntu 22.04.1 LTS`** 

<div align="center"><img src=./images/ubuntu2204.png width="450"/></div>

**TROUBLESHOOTING:** If you receive a *virtualization error*, go to [Entering The BIOS](#entering-the-bios) for more information.

&nbsp; &nbsp; &nbsp;

9. Enter a short username without spaces or special characters and a short, pin-like password. 
*NOTE:* You won't see any characters as you enter your password. This is a security feature. 

<div align="center"> <img src=./images/ubuntu-username.png width="650"/></div>

&nbsp; &nbsp; &nbsp;

10. Paste the command from **`Step 7`** into the terminal by right clicking in the terminal window. Press **`enter`** to run.

<div align="center"><img src=./images/ubuntu-install-command.png width="750"/></div>

&nbsp; &nbsp; &nbsp;

11. Soon after you start the script, you will be prompted for your password. Enter the password you created in **`Step 9`**.

<div align="center"><img src=./images/apt-password.png width="450"/></div>

&nbsp; &nbsp; &nbsp;

12. When you get to the `Starship` install prompt, press **`y`** and then `enter` to continue

<img src=./images/starship-prompt.png width="350"/>

&nbsp; &nbsp; &nbsp;

13. Enter the same password as **`Step 9`** for the following prompt:

<img src=./images/final-password.png width="350"/>

&nbsp; &nbsp; &nbsp;

14. **`Codium`** will pop up shortly after the final password. Jump to [Finishing Up](#finishing-up) to ensure your installation was successful. 


[Top](#hello-world)

## Finishing Up

Regardless of your operating system, once the installation is complete, your terminal should look like the following, and Codium should have popped up with a sidebar as below:

<div align="center"> <img src=./images/zsh-prompt.png width="500"/></div>

&nbsp;

<div align="center"> <img src=./images/install-success.png width="650"/></div>

&nbsp; &nbsp; &nbsp;

Now, check out [Terminal Tools](#terminal-tools) to learn more about our CLI (Command Line Interface) and git, or [Text Editor Tips](#text-editor-tips) to learn more about what our text editor has to offer.

Or, if you are ready to jump straight into some code, go to our [Hello World Programs](#hello-world-programs).

## Terminal Tools

### The Command Line Interface

[Linux Command Line](https://ubuntu.com/tutorials/command-line-for-beginners#1-overview)

[Top](#hello-world)

### Git

[Git Tutorial](https://www.w3schools.com/git/default.asp)

[Top](#hello-world)

## Text Editor Tips

Your text editor is your best friend while programming.  In this section, we'll get to know some more of its capabilities. This is far from an exhaustive list.  If you want to make the most out of your text editor, you should also consider checking out [Awesome VSCode](https://github.com/viatsko/awesome-vscode).

### Rendered Markdown

You have probably found it inconvenient to read this document in your web browser while using your terminal, hopping between reading it and running commands.  Luckily, this repository is now on your computer, and your text editor has a built in Markdown reader! 

In order to access it, first click on the **`README.md`** file on your left side panel. Then on Windows, press **`ctrl + \`** or **`cmd + \`** on MacOS to split your window. Then press **`ctrl + shift + v`** or **`cmd + shift + v`** to toggle the Markdown preview. Now you have the same document as the website right in your editor!

[Markdown was co-invented](https://en.wikipedia.org/wiki/Markdown) by a writer named [John Gruber](https://en.wikipedia.org/wiki/John_Gruber) and a programming prodigy named [Aaron Swartz](https://en.wikipedia.org/wiki/Aaron_Swartz). 

Gruber, a computer scientist and professional blogger, wanted an easier way to write formatted text than using a word processor or HTML. He worked together with Aaron in order to create the new language that we know today as Markdown. Gruber still [actively blogs](https://daringfireball.net/) using Markdown. 

Aaron, tragically, took his own life at the young age of 26 after a long battle with the Federal Government over hacking charges. You can see a movie about Aaron's life and his contributions to programming and internet activism in the movie [The Internet's Own Boy](https://www.youtube.com/watch?v=3Q6Fzbgs_Lg). 

Aaron's legacy lives on in much of his work. He was one of the co-founders of Reddit.  And his contributions to Markdown make it possible for people who don't program for a living to create easily formatted blogs.

Thanks to Aaron and John, you can see by the raw file, markdown is very clean to write relative to HTML. And in its rendered version, it has most of the features of marked up text from a word processor like Microsoft Word. 

Try scrolling the raw **`README`** to see how its code translates to our webpage. You'll be able to see a condensed version of Markdown's features in **`hello.md`**. Play around with it to make Markdown your own!

[Top](#hello-world)

### Popup Terminal

While we configured a separate terminal, our text editor also has a built in terminal which is very useful for using quick commands like compiling code. In order to toggle it from the bottom, press **``ctrl + ` ``** on Windows or **``cmd + ` ``** on Mac (this is the backtick button above the **`tab`** key). Now that we have our quick terminal, we can run any of our programs in the comfort of our text editor.

When you want to run the code in this repository, make sure you are in its root directory.  You can do this by typing:

```
cd hello-world
```

or the **c**hange **d**irectory command. Our shell will automatically output the list of directories and files in the new directory we enter. Then we will be able to run any executables from this directory.

[Top](#hello-world)

## Hello World Programs

### HTML, CSS, And Javascript

When people think about programming, typically they think about Computer Science or another STEM discipline. Often coding practitioners come from these fields, but many others come from a background in Design and the Arts. This section is dedicated to the oft forgotten programmers that make the internet a beautiful place!

HTML or HyperText Markup Language is the language that gives webpages their structure. HTML, along with Javascript, and CSS (or Cascading Style Sheets) form the tripod of web languages. HTML gives web pages their structure, CSS the style, and Javascript the logic.

This set of hello worlds is very interactive as you will be editing a website from the ground up.  As you go, you'll see the various ways HTML adds structure. And, you'll realize pretty quickly, raw HTML is pretty ugly. We'll jazz it up with some CSS. Finally, we'll make our webpage interactive with Javascript and you will learn how you can manipulate the DOM (or Document Object Model) using Javascript powered buttons.

Open **`hello.html`** in Codium, and then open your web browser of choice, pressing **`ctrl+o`** and navigating to it with your file explorer.

#### Web Language Extras

HTML, CSS, and Javascript may be the foundational languages of the internet, but if you choose to go further down this path, you'll learn much more powerful ways to create for the web. 

**React.js** and **Angular.js** are two javascript frameworks made by Facebook and Google respectively that power web applications like Netflix and Google Docs. **`Typescript`** is an advanced version of Javascript created by Microsoft which allows Javascript to be compiled like **`C++`**. 

Finally, **`Electron.js`** harnesses the power of **`Chromium`**, the Open Source component of Chrome, to create desktop applications like Spotify and Discord. Our humble text editor, **`Codium`**, is an example of an electron application!

You'll find that learning these core languages offer a tremendous amount of possibilities for creating new and exciting things as well as opening the door great career paths.  Happy coding!

[Top](#hello-world)

### Bash

Bash or the "Bourne Again Shell" is one of the primary tools for interacting directly with your computer's subsystems. It is incredibly powerful for automation and customization. Just about all servers and cloud instances run Linux, so if you ever want to work in the Cloud, you should learn bash.

**`hello.bash`** is allows you to pick from a list of 10 options to get information from your computer. Because of the interactivity of this program, and it's readability, you'll get a chance to learn programming syntax like **`for`** loops, **`case`** statements, and conditional arguments.

To start with **`hello.bash`**, open your full sized terminal. 

``` sh
cd hello-world
```

Then you'll be able to run **`hello.bash`** with:

``` sh
bash hello.bash
```

As you play with the program, try to follow along with what the code is doing from **`Chromium`**

#### System Level Programming Extras

As with the web languages, learning **`bash`** only scratches the surface of a vast array of kinds of programming. Because bash interfaces deeply with your computer, you can learn cryptography, how to jump from computer to computer using their IP addresses, and even how to host your HTML files on the web from your own computer!

**`Ansible`** and **`Jenkins`** are languages for automating bash-y tasks across an entire network of computers using proxies, web servers, and databases. **`Gitlab`** provides CI/CD (Continuous Integration and Continuous Deployment) tools that harness the power of bash in order to automate building and testing programs so they can be deployed as soon as they are uploaded. Finally, **`Bash`** is key to containerization and virtualization, two critical parts of Cloud based workflows such as **`AWS`** and **`Azure`**.

A successful **`bash`** programmer will gain a mastery of automating their own computer as well as remote ones. Our automated deployment scripts were primarily written in **`bash`**. In time, you will probably be able to write better ones!

[Top](#hello-world)

### Python

Python is a Swiss Army Knife language. It's strongest applications are in machine learning and data science, but it is capable of doing almost anything. While there are better languages for some applications, if you want to prototype, get a quick program out of the door, or don't know what might be better, python is your friend.

**`hello.py`** will show you how python can be used for plotting data.  Like the **`bash`** script, it is highly interactive allowing you to choose your own adventure. You will have the choice of generating one of two plots. A scatter plot or a contour plot. There are two sample sets of data for each. A third data file is left blank for each. Try to think of situation that would lend itself to these plots and see if you can create the data to model it!

If you open **`hello.py`** in your text editor, you'll notice it's quite short.  The majority of the code is for generating the prompts. The reason for this is that it relies on external packages to do the heavy lifting. Before you will be able to run the code, you'll need to install these packages. Luckily, the **`requirements.txt`** file has all of the packages listed. To use it, open your shell, change directories into **`hello-world`** and enter the following:

``` python
pip install -r requirements.txt
```

or

``` python
pip3 install -r requirements.txt
```

This will automatically install all of the necessary packages to run our code! This saves us the headache of what's called [Dependency Hell](https://en.wikipedia.org/wiki/Dependency_hell). Once these packages are installed, you're ready to run:

``` sh
python hello.py
```

#### Python Extras

Like our other languages, learning python lends itself to additional applications. **`TensorFlow`** is an Open Source Machine Learning library created by Google. Learning TensorFlow will allow you to train your own AI! **`Jupyter Notebooks`** is a python library for writing what is called [Literate Programming](https://en.wikipedia.org/wiki/Literate_programming). Literate programming allows you to embed prose in your code. Finally, with it's strength in Data Science, learning python can be the gateway for a career with just about any Fortune 500 company.

[Top](#hello-world)

### Language Comparison

Bash is probably the most difficult language to write of our examples.  It is very picky and the syntax doesn't lend itself to debugging easily. Between python and javascript, their read and writeability are up to taste and experience. 

The python way of creating code blocks is to require indentation. It's nice because it forces everyone to format their code the same making everyone's code uniform to read.

Javascript is a so called "C-like language." It's syntax follows a lineage of other languages like Java, Rust, or C++ who derive their syntax from C. The key feature of C-like languages is their curly brackets and the way they write functions. If you write one C-like language, learning another is easier.  

HTML and Markdown are both markup languages, providing structure to text.  HTML is predominantly used for webpages, while Markdown is a great choice for guides such as this one. Given the choice, most people will prefer markdown!

[Top](#hello-world)

### To Learn More:
* https://www.w3schools.com/html/default.asp
* https://www.w3schools.com/css/default.asp
* https://www.w3schools.com/js/default.asp
* https://manpages.org/bash
* https://www.w3schools.com/python/default.asp

[Top](#hello-world)

# Appendix

## Entering the BIOS 
A virtualization error will look similar to this:

![BIOS-error.png](images/BIOS-error.png "BIOS Error")

This will require you to enable virtualization in the BIOS. Entering the BIOS involves rebooting and mashing the correct key before Windows starts. [Click Here](https://www.lifewire.com/enter-bios-on-windows-10-5095780) or [here](https://www.lifewire.com/bios-setup-utility-access-keys-for-popular-computer-systems-2624463) to see a list of common keys per vendor. If none of these work, try doing a web search for your exact make and model + 'BIOS key'. 

Before you attempt to enter the BIOS, take a picture with your phone of these instructions so you know what to do upon entering the BIOS. UEFI will have more modern interface that is easier to navigate. If your computer has a BIOS it will look something like this:

![BIOS-virtualization.png](images/BIOS-virtualization.png "BIOS Virtualization Picture")

Regardless of if your computer uses UEFI or BIOS, you want to find a menu that allows you to enable virtualization. It is typically under 'Advanced' or 'CPU' settings. Once you enable the BIOS be sure to save before you choose to exit or reboot. 

If you have trouble enabling virtualization, the Help Desk will be able to help you. Tell them you want to enable virtualization in your BIOS and the key you pressed to enter the BIOS. They'll be able to fix this very quickly. 

[Top](#hello-world)

## What We Installed

Our installation was completed with 100% Free and Open Source software. Below is a short, but incomplete, list of the software that made it possible.

### Editor, Linters, and Code Completion

* ![codium](https://github.com/VSCodium/vscodium)
* ![eslint](https://github.com/eslint/eslint)
* ![csslint](https://github.com/CSSLint/csslint)
* ![remark-lint](https://github.com/remarkjs/remark-lint)
* ![pycodestyle](https://github.com/PyCQA/pycodestyle)
* ![jedi](https://github.com/davidhalter/jedi)

### Package Managers

* ![apt](https://github.com/Debian/apt)
* ![python-pip](https://github.com/pypa/pip)
* ![Brew](https://docs.brew.sh/Installation)
* ![Chocolatey](https://chocolatey.org/install#individual)
* ![npm](https://github.com/npm/cli)

### CLI Tools

* ![bat](https://github.com/sharkdp/bat)
* ![starship](https://github.com/starship/starship)
* ![exa](https://github.com/ogham/exa)
* ![ripgrep](https://github.com/BurntSushi/ripgrep)
* ![fd-find](https://github.com/sharkdp/fd)
* ![bash](https://www.gnu.org/software/bash/)
* ![zsh](https://www.zsh.org/)
* ![git](https://git-scm.com/)
* ![tldr](https://github.com/tldr-pages/tldr)
* ![mandb](https://www.man7.org/linux/man-pages/man8/mandb.8.html)
* ![fontconfig](https://github.com/behdad/fontconfig)
* ![unzip](https://github.com/LuaDist/unzip)

[Top](#hello-world)

## Acknowledgements

This repository was created by the Washington & Lee University Physics Department. A special thanks to the thousands of programmers around the world who were kind enough to release their code under FOSS licenses. 

[Top](#hello-world)

## WSH

windows users will need to do the following to avoid bugs we've been running into:

1. Open your terminal 
2. `cd` into this directory
3. Copy the following command into your terminal and run it

``` sh
mkdir -p ~/.local/bin; cp wsh ~/.local/bin; cd ~/.local/bin; dos2unix.exe wsh; chmod 777 wsh; echo 'PATH="$PATH:$HOME/.local/bin'; cd -; zsh
```

If this didn't work, open an admin powershell and run `choco install dos2unix`. Then try again from step 1.

From now on, whenever you see a script ending in `.bash` you will run `wsh script.bash` rather than `bash script.bash`
