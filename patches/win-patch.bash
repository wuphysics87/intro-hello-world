#!/usr/bin/env bash

sudo apt install -y xserver-xorg dos2unix unix2dos

[ ! "$(command -v starship)" ] && curl -sS https://starship.rs/install.sh | sh

win_username="$(powershell.exe '$env:UserName')"


term_path="/mnt/c/Users/${win_username%?}/AppData/Local/Packages"
term_name=`/bin/ls "$term_path" | grep Terminal`
settings_path="$term_path/$term_name/LocalState"

cp "shell/term-settings.json" "$settings_path"

cp "shell/settings.json" "/mnt/c/Users/${win_username%?}/AppData/Roaming/VSCodium/User/settings.json"

rm -f "$HOME/.profile" "$HOME/.zshrc"
cp "shell/profile" "$HOME/.profile"
cp "shell/zshrc" "$HOME/.zshrc"
