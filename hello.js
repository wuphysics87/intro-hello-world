// This is useful for debugging.
// While in your browser viewing our page, press F12 in Firefox and Chromium
// Then check the console tab. You'll see Hello Universe! printed there'
console.log('Hello Universe!');

// Since you are here, check out the Inspector tab.
// You can take a closer look at any object in the DOM (Document Object Model)
// https://www.w3schools.com/whatis/whatis_htmldom.asp

const mybutton = document.querySelector("#submit-button");

mybutton.addEventListener("click", helloname);

function helloname() {
  console.log("something")
  const name = document.getElementById('tt').value;
  document.getElementById("your-name").innerHTML = ["Hello ", name, "!"].join('');

//  var elem = document.getElementById(id);
//
//  elem.innerHTML = "HELLO!"

}

// This code is for our Lorem Ipsum hider. Note the #references to the id's in the DOM.
const article = document.querySelector("#lorem");
const button = document.querySelector("#read-more");

button.addEventListener("click", readMore);

function readMore() {
     if (article.className == "open") {
       // Read less
     article.className = "";
     button.innerHTML = "Show more";
   } else {
     article.className = "open";
     button.innerHTML = "Show less";
   }
};

// Color change button.  Check out the reference tags.
// You won't need to edit it to add more buttons.
// That's all in the HTML and CSS 8)
const name = document.querySelector(".name");
const btn = document.querySelector(".colorbutton");

btn.addEventListener("click", changeColor);

function changeColor(color){
  name.style.color = color;
}

