#!/usr/bin/env bash

heading() {

        printf "\n===================================\n"
        printf "$1 \n"
        printf "===================================\n\n"
}

quitting(){
    heading "   Quiting Program In..."
    countdown
    heading "    Goodbye!"
    exit
}

countdown(){
    printf "    3...\n"; sleep 1.5;
    printf "    2...\n"; sleep 1;
    printf "    1...\n"; sleep 1;
}

next_choice(){
       heading "      Would You Like To"

        printf " \
   Make Another Choice.......1\n \
   Quit......................2\n\n: "
        read -r choice

        if [[ "$choice" == "1" ]]; then
            menu
        elif [[ "$choice" == "2" ]]; then
            quitting
        else
            printf "\n\n Invaid Choice! Try Again!\n\n"
            sleep 2
            next_choice
        fi
}


get_os_info(){

	if [ -d "/mnt/c" ]; then
        printf "You Are Running Windows"

	elif [ -d "/Applications" ]; then
        if [ ! "$(sysctl -n machdep.cpu.brand_string | grep Intel)" ]; then
            cpu="Apple Silicon"
        else
            cpu="Intel"
        fi
		    printf "You Are A Mac User With\n An %s Processor" "$cpu"
    fi

}

name_print(){
    heading "    What Is Your Name?"
    printf ": "
    read -r my_name
    heading "         Hello $my_name!\n    How Many Times Would You\n    Like To See Your Name?";
    printf ": "
    read -r name_count

    if [[ ! "$name_count" =~ ^[[:digit:]]+$ ]]; then
        echo "ERROR $name_count IS NOT A NUMBER!"
        printf "Returning To Menu In ...\n"
        name_print

    elif (( $name_count > 100 )); then
        echo "OVERLOAD!!! Pick a smaller number!"
        name_print

    else
        for ((i=1; i<=$name_count; i++)); do
            echo "$i Your Name Is: $my_name"
        done

    fi
}

menu(){
     clear
     heading "    Choose What You'd Like To Do:"
     printf " \
   Show Current Path.........0\n \
   See Directory Tree........1\n \
   See The Date And Time.....2\n \
   Check Your Disk Space.....3\n \
   Get OS Information........4\n \
   Print Your Name N Times...5\n \
   Show Your Username........6\n \
   Get Your IP Address.......7\n \
   Get Computer Name.........8\n \
   Quit......................9\n\n: "
        read -r choice

        clear
        case "$choice" in
            0) heading "  Your Current Directory Is..."; printf "   $(pwd)\n";;

            1) heading "  Listing Directory Tree..."; exa -T -L 1 ;;

            2) heading "     The Date And Time Are..."; printf "   $(date)\n";;

            3) heading "  Here's Your Disk Info..."; df -h;;

            4) heading "     Getting OS Info..."; get_os_info;;

            5) name_print;;

            6) heading "    Your User Name Is..."; printf "     $USER\n";;

            7) heading "    Your IP Address Is..."

	           if [ -d "/mnt/c" ]; then
                   printf "\n $(hostname -I) \n"
               else
                   printf "\n $(ipconfig getifaddr en0) \n"
               fi;;

            8) heading "  Your Computer's Name Is..."; printf "   %s" "$HOSTNAME\n";;
            9) quitting;;
            *) printf "\nIllegal choice! Try Again!\n\n"; menu;;

        esac

        next_choice

}

clear

heading "      Welcome hello.bash!\n         Starting In...."

countdown

menu
