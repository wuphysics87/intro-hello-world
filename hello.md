# Heading 1
## Heading 2
### Heading 3
#### Heading 4
##### Heading 5
###### Heading 6


Paragraphs of text can be made easily by making.

An empty line between lines!

## Lists

### Unordered
* Item 1
  * Item A
  * Item B
* Item 2
+ Item 3
- Item 4

### Ordered
1. Item 1
    1. Item A
    1. Item B
1. Item 2

1. Item J
2. Item K
3. Item L

## Text Styles

* *italic*
* **BOLD**
* ~~strikethrough~~
* `code`
*  [link](https://vignette.wikia.nocookie.net/zelda/images/0/05/Link_Artwork_1_(A_Link_to_the_Past).png/revision/latest?cb=20150802053414&path-prefix=pt-br)



> Block Quote
```sh
CODE BLOCK
```

* Inline Image of Twinkie Doge!

![Inline Image](https://i.imgur.com/MakT5Cf.jpeg)

## Tables

|Column A |Column B |
--- | ---|
|1A|1B|
|2A|2B|

## HTML

Markdown also allows you to write `HTML`. For example, check out the heading of [README.md](../README.md). It uses `HTML` to center itself.
