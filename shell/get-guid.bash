#!/usr/bin/env bash


win_username="$(powershell.exe '$env:UserName')";
term_path="/mnt/c/Users/${win_username%?}/AppData/Local/Packages"
term_name=`/bin/ls "$term_path" | grep Terminal`


settings_path="$term_path/$term_name/LocalState"

cp "$settings_path/settings.json" "$settings_path/settings.json.bak"

while read line; do

    if [[ "$line" == *"guid"* ]]; then
        guid=$line
    fi

    if [[ "$line" == *"Ubuntu 22.04.1"* ]]; then
        break
    fi
done < "$settings_path/settings.json.bak"

echo "$guid"

my_number=`echo "$guid" | sed -E "s/.*\"\{(.*)\}\".*/\1/g"`

echo "$my_number"
sed -E "s/.*defaultProfile.*/     \"defaultProfile\": \"\{$my_number}\",/" "$settings_path/settings.json.bak" >> better-thing.json
